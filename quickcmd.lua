-- be careful, encoding is CP1251
script_author("THERION")
script_description("������ ������� ��� �������")

-- change these if you want
local GALAXY_CMD_FORMAT = "/su %s %s"

local CMD = "qcmd"
local DIALOG_TITLE = "{FFFFFF}�������:"

-- ����� ���� ��� ��������� ����������� �������
local cmd_list = {
   dr   = {"1 ���������", "��������� -> /%s [id]"},
   mt   = {"1 ���������", "��������� -> /%s [id]"},
   pl   = {"1 �������", "������� -> /%s [id]"},
   dm   = {"3 ���������", "��������� -> /%s [id]"},
   mdm  = {"6 �������� DM", "�������� DM -> /%s [id]"},
   izd  = {"6 ��������������", "�������������� -> /%s [id]"},
   pp   = {"3 ������ �����������/���������", "������ �����������/��������� -> /%s [id]"},
   xm   = {"3 �������", "������� -> /%s [id]"},
   osk  = {"6 �����������", "����������� -> /%s [id]"},
   kill = {"6 �������� ����������", "�������� -> /%s [id]"},
   yd   = {"3 ���� �� ��������", "���� �� �������� -> /%s [id]"},
   pdd  = {"2 ��������� ���", "��������� ��� -> /%s [id]"},
   apa  = {"6 ���", "��� -> /%s [id]"},
   db   = {"6 Drive-By (DB)", "Drive-By (DB) -> /%s [id]"},
   vd   = {"2 ���������", "��������� -> /%s [id]"}
}

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

function main()
   repeat wait(0) until isSampAvailable()

   print("������ ����������: /qcmd")
   sampRegisterChatCommand(CMD, show_cmd_list)

   for cmd, data in pairs(cmd_list) do
      sampRegisterChatCommand(cmd,
      function(args)
         local id = tonumber(args)
         if id and sampIsPlayerConnected(id) then
            sampSendChat(string.format(GALAXY_CMD_FORMAT, args, data[1]))
         else
            log("���������, ��� �� ����� ���������� ID")
         end
      end)
   end

   wait(-1)
end

function show_cmd_list()
   local cmd_info = {}
   
   for cmd, data in pairs(cmd_list) do
      local info = string.format(data[2], cmd)
      table.insert(cmd_info, info)
   end

   local str = table.concat(cmd_info, "\n")
   sampShowDialog(dialogId, DIALOG_TITLE, str, "�������", "", 0)
end
