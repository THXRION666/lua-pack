script_author("THERION")
script_description("Allows you to change radar positions")

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

-- prints out if user does not have some of the needed files
local function catch_exception(status, path)
   local msg = string.format('File not found: "%s". Shutting down..', path)
   if not status then log(msg) end
   assert(status, msg)
end

do --Loading libraries
   local list = {
      memory     = "memory",
      inicfg     = "inicfg",
   }

   local result = nil
   for var, path in pairs(list) do
      result, _G[var] = pcall(require, path)
      catch_exception(result, path)
   end
end 

local ini = inicfg.load({
   global = {
      script_state = true
   },
   bounds = {
      pos_x  = 10,
      pos_y  = 90,
      size_x = 80,
      size_y = 80
   }
}, thisScript().name .. ".ini")

local function set_size(x, y)
   memory.setint32(0x866B74, representFloatAsInt(x), true)
   memory.setint32(0x866B78, representFloatAsInt(y), true)
end

local function set_pos(x, y)
   memory.setint32(0x858A10, representFloatAsInt(x), true)
   memory.setint32(0x866B70, representFloatAsInt(y), true)
end


local function cmd_execute(args)
   if args == "show" then
      local format = "Position( x: %d, y: %d ). Size( x: %d, y: %d )"
      local msg = string.format(format, ini.bounds.pos_x, ini.bounds.pos_y, ini.bounds.size_x, ini.bounds.size_y)
      log(msg)
      return
   end
   if args == "help" then
      log("/radar <size/position> <x> <y> - change radar size/position")
      log("/radar <on/off> - define if script is on or off by default")
      log("/radar show - print current radar position and size")
      log("/radar save - save configuration")
      return
   end
   if args == "save" then
      if inicfg.save(ini, thisScript().name .. ".ini") then 
         log("Settings are successfully saved") 
      end
      return
   end

   local arg_list = {}
   for token in args:gmatch("[^%s]+") do
      table.insert(arg_list, token)
   end

   if arg_list[1] == "on" then 
      ini.global.script_state = true
      return
   end
   if arg_list[1] == "off" then 
      ini.global.script_state = false
      return
   end
   if arg_list[1] == "size" and tonumber(arg_list[2]) and tonumber(arg_list[3]) then
      ini.bounds.size_x = tonumber(arg_list[2])
      ini.bounds.size_y = tonumber(arg_list[3])
      set_size(ini.bounds.size_x, ini.bounds.size_y)
      return
   end
   if arg_list[1] == "position" and tonumber(arg_list[2]) and tonumber(arg_list[3]) then
      ini.bounds.pos_x = tonumber(arg_list[2])
      ini.bounds.pos_y = tonumber(arg_list[3])
      set_pos(ini.bounds.pos_x, ini.bounds.pos_y)
      return
   end
end

function main()
   do
      if not doesDirectoryExist("moonloader\\config") then 
         createDirectory("moonloader\\config") 
      end
      if not doesFileExist(thisScript().name .. ".ini") then 
         inicfg.save(ini, thisScript().name .. ".ini") 
      end
   end

   repeat wait(0) until isSampAvailable()

   set_size(ini.bounds.size_x, ini.bounds.size_y)
   set_pos(ini.bounds.pos_x, ini.bounds.pos_y)

   sampRegisterChatCommand("radar", cmd_execute)

   print("commands: /radar help")
   
   wait(-1)
end