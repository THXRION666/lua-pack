script_name("FancyFonts")
script_author("THERION")
script_description("Forces gametext style to 4")

local sampev = require("samp.events")

sampev.onDisplayGameText = function(style, time, text)
	return {4, time, text}
end

function main()
   repeat wait(0) until isSampAvailable()

	wait(-1)
end