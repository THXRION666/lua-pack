script_name("crosshair_dot")
script_author("THERION")

-- config
local corners = 12
local color = 0xFFFFFFFF
local size = {
   x = 4,
   y = 4,
}
-- config

-- const
local CAMERA_POINTER = 0x00B6F028
local FIRST_PERSON_MODES = {
   [0x7] = "MODE_SNIPER",
   [0x8] = "MODE_ROCKETLAUNCHER",
   [0x10] = "MODE_1STPERSON",
   [0x33] = "MODE_ROCKETLAUNCHER_HS",
}
--

local function getCameraMode()
   local cameraMode = readMemory(CAMERA_POINTER + 0x180, 1, false)
   return cameraMode
end

local function getCrosshairPositionOnScreen()
   local resolution_x, resolution_y = getScreenResolution()
   return resolution_x * 0.5299999714, resolution_y * 0.4
end

local function onEveryFrame()
   local cameraMode = getCameraMode()
   if FIRST_PERSON_MODES[cameraMode] then
      return
   end

   local crosshairX, crosshairY = getCrosshairPositionOnScreen()
   renderDrawPolygon(crosshairX, crosshairY, size.x, size.y, corners, 0, color)
end

function main()
   while true do
      wait(0)
      onEveryFrame()
   end
end
