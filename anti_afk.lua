local memory = require("memory")

local isAfk = false

local function switchAFK()
   isAfk = not isAfk
   if isAfk then
      printString("~g~AFK", 2000)
      memory.setuint8(0x747FB6, 1, false)
      memory.setuint8(0x74805A, 1, false)
      memory.fill(0x74542B, 0x90, 8, false)
      memory.fill(0x53EA88, 0x90, 6, false)
   else
      printString("~r~AFK", 2000)
      memory.setuint8(0x747FB6, 0, false)
      memory.setuint8(0x74805A, 0, false)
      memory.hex2bin("0F 84 7B 01 00 00", 0x74542B, 8)
      memory.hex2bin("50 51 FF 15 00 83 85 00", 0x53EA88, 6)
   end
end

function main()
   repeat wait(0) until isSampAvailable()
   sampRegisterChatCommand("afk", switchAFK)
   wait(-1)
end
