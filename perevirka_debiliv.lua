script_author("THERION")

-- lib
require("moonloader")
local encoding = require("encoding")
encoding.default = "CP1251"
--

-- config
local ACTIVATION_KEY = VK_R
local FONT_NAME = "Arial"
local FONT_SIZE = 20
local FONT_FLAGS = 0x04
local FONT_COLOR = -1
local GAP_BETWEEN_LINES = 6
local ACTIVITIES = {
   BEING_CRINGE = encoding.UTF8:decode("{FF4040}десь пройобується"),
   BEING_AFK = encoding.UTF8:decode("{80FF40}поряд, але AFK"),
   BEING_A_FINE_BOY = encoding.UTF8:decode("{40FF40}на місці"),
}
--

-- const
local GALAXY_RPG_IP_LIST = {
   ["80.66.71.70"] = 1,
   ["176.32.39.199"] = 2,
   ["176.32.39.198"] = 3,
}
--

local function getPlayerActivity(id)
   if not sampGetCharHandleBySampPlayerId(id) then
      return ACTIVITIES.BEING_CRINGE
   end
   if sampIsPlayerPaused(id) then
      return ACTIVITIES.BEING_AFK
   end

   return ACTIVITIES.BEING_A_FINE_BOY
end

local function forNumbersInRange(range, callback)
   for i = 0, range do
      callback(i)
   end
end

local function getThemAllAndTheirActivitiesToo()
   local result = {}
   forNumbersInRange(1000, function(id)
      if not sampIsPlayerConnected(id) then
         return
      end

      local name = sampGetPlayerNickname(id)
      if not name:find("pernul.i") then
         return
      end

      local activity = getPlayerActivity(id)
      table.insert(result, ("%s: %s"):format(name, activity))
   end)
   return result
end

local dxFont = nil
local function onInit()
   local ip, _ = sampGetCurrentServerAddress()
   if not GALAXY_RPG_IP_LIST[ip] then
      script.this:unload()
      return
   end

   dxFont = renderCreateFont(FONT_NAME, FONT_SIZE, FONT_FLAGS)
end

local function onEveryFrame()
   if not isKeyDown(ACTIVATION_KEY) then
      return
   end

   if sampIsChatInputActive() or isSampfuncsConsoleActive() or sampIsDialogActive() then
      return
   end

   local resolutionX, resolutionY = getScreenResolution()
   local middleOfScreenX, middleOfScreenY = resolutionX / 2, resolutionY / 2
   local list = getThemAllAndTheirActivitiesToo()

   for index, text in ipairs(list) do
      local textWidth = renderGetFontDrawTextLength(dxFont, text)
      local posX = middleOfScreenX - textWidth / 2
      local gapsFromCenter = (#list + 1) / 2.0 - index
      local linesFromCenter = #list / 2.0 - index + 1
      local posY = middleOfScreenY - gapsFromCenter * GAP_BETWEEN_LINES - linesFromCenter * renderGetFontDrawHeight(dxFont)
      renderFontDrawText(dxFont, text, posX, posY, FONT_COLOR)
   end
end

function main()
   repeat wait(0) until isSampAvailable()
   onInit()

   while true do
      wait(0)
      onEveryFrame()
   end
end
