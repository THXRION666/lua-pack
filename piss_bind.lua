require("moonloader")

local START_ANIMATION_COMMAND = "/piss"
local START_ANIMATION_KEY = VK_X
local RESET_ANIMATION_KEY = VK_F
local APPROXIMATE_PING = 100

local function onEveryFrame()
   if not isKeyJustPressed(START_ANIMATION_KEY) then
      return
   end
   
   if sampIsChatInputActive() or isSampfuncsConsoleActive() or sampIsDialogActive() then
      return
   end

   sampSendChat(START_ANIMATION_COMMAND)
   wait(APPROXIMATE_PING)

   setVirtualKeyDown(RESET_ANIMATION_KEY, true)
   wait(0)
   setVirtualKeyDown(RESET_ANIMATION_KEY, false)
end

function main()
   repeat wait(0) until isSampAvailable()

   while true do
      wait(0)
      onEveryFrame()
   end
end