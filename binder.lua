-- AUTHOR: THERION
require("moonloader")
local encoding = require("encoding")
encoding.default = "CP1251"

local bindings = {
	{ primary = VK_NUMPAD7, command = "/eat" },
	{ primary = VK_NUMPAD8, command = "/usemeds" },
	{ primary = VK_NUMPAD9, command = "/udc" },
	{ primary = VK_NUMPAD5, command = "/get clothes" },
	{ primary = VK_NUMPAD2, command = "/crack" },
	{ primary = VK_NUMPAD0, command = "/unrentcar" },
   -- { primary = VK_X, command = "/piss" },
}

local function isBindingActivated(binding, triggerKey)
   if sampIsChatInputActive() or isSampfuncsConsoleActive() or sampIsDialogActive() then
      return false
   end

   if binding.extra and not isKeyDown(binding.extra) then
      return false
   end

   if triggerKey ~= binding.primary then
      return false
   end

   return true
end

local windowMessages = require("windows.message")
function onWindowMessage(msg, wparam)
   if msg ~= windowMessages.WM_KEYDOWN and msg ~= windowMessages.WM_SYSKEYDOWN then
      return msg, wparam
   end

   for _, binding in ipairs(bindings) do
      if isBindingActivated(binding, wparam) then
         sampSendChat(binding.command)
      end
   end
end
